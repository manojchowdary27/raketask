class TopHolding
  include Mongoid::Document
  field :equity, type: String
  field :sector, type: String
  field :qty, type: String
  field :Value, type: String
  field :percentage, type: String
end
