# README #


### What is this repository for? ###

* This rails app contains a rake task.It can scrap the portfolio section following html file 
* [Website] (http://www.moneycontrol.com/india/mutualfunds/mfinfo/portfolio_holdings/MSB079).
* It scraps the HTML using nokogiri gem.
* It stores the scrapped content in Mongodb database using mongoid gem

###  Set up Instructions ###
* This app uses mongodb database
* The database configuration is in raketask file (lib/task/scrap.rake).

* Here I used the following database and colletion configuration.
 client = Mongo::Client.new('mongodb://127.0.0.1:27017/test')
    collection = client[:top_holdings]